#!/usr/bin/env python

import sys
import paramiko
from paramiko.client import SSHClient

from tabulate import tabulate
from firewall import FIREWALL_PUBLIC_IPV4, FirewallPort

import utilities.common as common
from utilities.common import VALID_MS_LHS, az_cli
from utilities.common import VMResourceNames


def check_vm_prereqs(ms_lh: str):
    """Checks that each of the virtual network and image exists

    Args:
        ms_lh (str): [description]

    Returns:
        [type]: [description]
    """

    # Can't put a azregion filter in any of the following queries unfortunately.
    # The 'Location' column of the returned ...list queries can obtain that though

    # Check that the image doesn't exist - we create it as part of this script
    img_name = VMResourceNames.get_img_name(ms_lh)
    exists, *_ = az_cli([
        "image", "show",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", img_name,
    ])
    if exists:
        raise LookupError(f"Image '{img_name}' already exists in Azure under xm-rg")

    # Check that virtual network exists
    vnet_name = VMResourceNames.get_vnet_name(ms_lh)
    exists, *_ = az_cli([
        "network", "vnet", "show",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", vnet_name,
    ])
    if not exists:
        raise LookupError(f"VNet '{vnet_name}' doesn't exist in Azure under xm-rg")

    # Check that public ipv4 exists
    public_ipv4_name = VMResourceNames.get_public_ipv4_name(ms_lh)
    exists, *_ = az_cli([
        "network", "public-ip", "show",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", public_ipv4_name
    ])
    if not exists:
        raise LookupError(f"Public IPv4 address '{public_ipv4_name}' doesn't exist in Azure under xm-rg")

    # Check that network security group exists
    nsg_name = VMResourceNames.get_nsg_name(ms_lh)
    exists, *_ = az_cli([
        "network", "nsg", "show",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", nsg_name
    ])
    if not exists:
        raise LookupError(f"Network Security Group '{nsg_name}' doesn't exist in Azure under xm-rg")


def create_image(ms_lh: str, vhd_name: str):

    img_name = VMResourceNames.get_img_name(ms_lh)
    vhd_source = f"https://{VMResourceNames.get_storage_acct_name(ms_lh)}.blob.core.windows.net/lh-images/{vhd_name}"
    img_location = common.get_azure_region(ms_lh)

    was_cmd_successful, result, _ = az_cli([
            "image", "create",
            "--resource-group", common.RESOURCE_GROUP,
            "--name", img_name,
            "--source", vhd_source,
            "--location", img_location,
            "--os-type", "Linux",
            "--hyper-v-generation", "V1",
            "--storage-sku", "Premium_LRS",
            "--os-disk-caching", "ReadWrite",
            "--zone-resilient", "false",
            "--tags", "team=xm", f"purpose=msmigrate-lh-{ms_lh}",
        ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create VM image '{img_name}'. Error: {result}")


def create_vm(ms_lh: str, no_public_ip: bool) -> dict:
    """
    
    Refer to: https://docs.microsoft.com/en-us/cli/azure/vm?view=azure-cli-latest#az_vm_create
    for all "az vm create ..." options

    Args:
        ms_lh (str): [description]

    Returns:
        dict: [description]
    """

    resource_location = common.get_azure_region(ms_lh)
    img_name = VMResourceNames.get_img_name(ms_lh)
    vm_name = VMResourceNames.get_vm_name(ms_lh)
    vnet_name = VMResourceNames.get_vnet_name(ms_lh)
    subnet_name = VMResourceNames.get_subnet_name(ms_lh)
    public_ipv4_name = VMResourceNames.get_public_ipv4_name(ms_lh)
    private_ipv4 = common.get_static_private_ipv4(ms_lh)
    nsg_name = VMResourceNames.get_nsg_name(ms_lh)
    diagnostics_storage_acct = VMResourceNames.get_storage_acct_name(ms_lh)
    tag_purpose = f"purpose=msmigrate-lh-{ms_lh}"

    successful, result, exit_code = az_cli([
        "vm", "create",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", vm_name,

        # The following are all optional args for VM creation but not for our use case.

        #### Basics ####
        # "--availability-set", "None", # No infrastructure redundany required - THIS DIDN'T WORK. Errored out with: Availability set 'None' does not exist.
        "--image", img_name,
        "--location", resource_location,
        "--priority", "Regular", # Change to "Spot" for a spot instance or "Regular" for pay-as-you-go
        "--size", "Standard_E4s_v4", # ~ $ Run 'az vm list-sizes -l <location>' for alternatives, I hit quota limits when using Regular instances

        # Authentication - WARNING: I'm being super bad here. Credentials should be passed as env vars (ideally) or CLI args
        "--authentication-type", "password",
        "--admin-username", "localadmin",
        "--admin-password", "pLzw03k12345",

        #### Disks ####
        # Not sure what to put in here

        #### Networking ####

        # I cant see a way to add public inbound ports through this. It might have to be done later
        "--vnet-name", vnet_name,
        "--subnet", subnet_name,
        "--private-ip-address", private_ipv4,
        "--public-ip-address", "" if no_public_ip else public_ipv4_name,
        "--nsg", nsg_name,
        

        #### Management ####
        "--boot-diagnostics", diagnostics_storage_acct, # I think i had to add this to get a local console to the VM (it was ages ago so i've forgotten)
        
        #### Advanced ####

        #### Tags ####
        "--tags", "team=xm", tag_purpose
    ])

    if not successful:
        raise EnvironmentError(f"Failed to create VM. There are likely to be dependent resources that you'll need to manually clean up." \
                f" Use the tag '{tag_purpose}' to find them. Error: {result}")

    # For example, this creates the dependent resouces:
    #   lh-20q3.2-vm-dublin (Virtual Machine)
    #   lh-20q3.2-vm-dublin_disk1_8c7fa60bd9d541fc9 (Disk)
    #   lh-20q3.2-vm-dublinVMNic (network interface)

    return result


def set_firewall_external_address(ms_lh: str):
    """Should really only be run if there is no public ip

    The address of the firewall MUST be added when running behind
    the firewall else MI will not work.

    Args:
        ms_lh (str): [description]
    """

    firewall_address = FIREWALL_PUBLIC_IPV4
    firewall_ssh_port = FirewallPort.ssh_for(ms_lh)
    firewall_api_port = FirewallPort.https_for(ms_lh)
    firewall_vpn_port = FirewallPort.node_vpn_for(ms_lh)
    firewall_mi_vpn_port = FirewallPort.mi_vpn_for(ms_lh)
    
    ssh = paramiko.SSHClient()    
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # This is pretty bad from a security POV
    ssh.connect(firewall_address, port=firewall_ssh_port, username="localadmin", password="pLzw03k12345")

    cmd = f'''var e !append lighthouse_configurations[0].system.net.external_endpoints map
set {{e}}.address "{firewall_address}"
set {{e}}.api_port {firewall_api_port}
set {{e}}.vpn_port {firewall_vpn_port}
set {{e}}.mi_vpn_port {firewall_mi_vpn_port}
push'''
    
    # Set the external address to the firewall
    _, ssh_stdout, _ = ssh.exec_command(f"echo -e '{cmd}' | ogconfig-cli -e 2>&1")
    exit_status = ssh_stdout.channel.recv_exit_status()
    if exit_status != 0:
        ssh.close()
        raise EnvironmentError(f"Failed to set external address on the LH. Exit status = {exit_status}")


def enable_root_user(ms_lh: str, no_public_ip: bool):
    """Enables the root user and sets the password

    Args:
        ms_lh (str): [description]
        no_public_ip (bool): [description]
    """

    ssh_address = FIREWALL_PUBLIC_IPV4 if no_public_ip else common.get_static_public_ipv4(ms_lh)
    ssh_port = FirewallPort.ssh_for(ms_lh) if no_public_ip else 22

    ssh = paramiko.SSHClient()    
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # This is pretty bad from a security POV
    ssh.connect(ssh_address, port=ssh_port, username="localadmin", password="pLzw03k12345")
    
    cmd =f'''set users[0].enabled true
push
'''
    _, ssh_stdout, _ = ssh.exec_command(f"echo -e '{cmd}' | ogconfig-cli -e 2>&1")
    if ssh_stdout.channel.recv_exit_status() != 0:
        ssh.close()
        raise EnvironmentError("Failed to enable the root user")

    # Set the root user password
    _, ssh_stdout, _ = ssh.exec_command(f"ogpasswd --username root --password pLzw03k12345")
    if ssh_stdout.channel.recv_exit_status() != 0:
        ssh.close()
        raise EnvironmentError("Failed to set root user password")

    ssh.close()


def main():
    parser = common.get_common_cli_argparser(description=f"""
Create one of {VALID_MS_LHS} VM's in Azure and boot it up.\n"

Assumes:"
    - that the vhd is in a blob container within an storage account in the region where the vm is to be created\n"
    - the virtual network exists and is in the region where the vm is to be created
"""
    )

    parser.add_argument("-v", "--vhd", type=str, default="lighthouse-20.Q4.2.azure.vhd", dest="vhd_name",
            help="The name of the vhd within the blob container")
    parser.add_argument("--no-public-ip", default=False, dest="no_public_ip", action="store_true",
            help="If provided, the Lh boots without a public IPv4 address.")
    parser.add_argument("--enable-root-user", default=False, dest="enable_root_user", action="store_true",
            help="If provided, the 'root' user is enabled and the password set to 'pLzw03k12345'")

    args = parser.parse_args()

    # Check that this vm doesn't currently exist
    no_public_ip = args.no_public_ip
    enable_root = args.enable_root_user
    ms_lh = args.ms_lh
    vhd_name = args.vhd_name

    ms_lh_vm_name = VMResourceNames.get_vm_name(ms_lh)
    print(f"Checking if VM '{ms_lh_vm_name}' currently exists...")
    was_cmd_successful, *_ = az_cli([
        "vm", "show",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", ms_lh_vm_name
    ])
    if was_cmd_successful:
        sys.exit(f"VM '{ms_lh_vm_name}' already exists. Leaving config unchanged")

    print(f"VM '{ms_lh_vm_name}' is new.")
    print("Checking VM preqrequisites...")

    try:
        check_vm_prereqs(ms_lh)
    except LookupError as error:
        sys.exit(f"VM '{ms_lh_vm_name}' prerequisites unsatisfied: {error}")

    print(f"VM prerequisites satisfied. Creating VM image from '{vhd_name}'...")

    try:
        create_image(ms_lh, vhd_name)
    except EnvironmentError as error:
        sys.exit(f"VM iamge creation failed: {error}")

    print('Image created successfully. Creating VM (this will take awhile, check Virtual Machines on portal.azure.com to see the progress)...')

    try:
        result = create_vm(ms_lh, no_public_ip)
    except EnvironmentError as error:
        sys.exit(f"VM '{ms_lh_vm_name}' creation failed: {error}")
    
    print(f"Successfully created VM '{ms_lh_vm_name}'")

    # Create array of arrays e.g. [[key1, val1], [key2, val2], ...] for tabulator
    result_for_tabulator = list(map(list, result.items()))
    print(tabulate(result_for_tabulator))

    # Initialize a few things on the LH
    if no_public_ip:
        print("LH doesn't have a public IP. Adding firewall address and ports on LH...")
        try:
            set_firewall_external_address(ms_lh)
        except EnvironmentError as err:
            sys.exit(f"Failed to set external endpoints on LH: {err}")

    if enable_root:
        print("Enabling root user...")
        try:
            enable_root_user(ms_lh, no_public_ip)
        except EnvironmentError as err:
            sys.exit(f'Failed to enable root user on LH: {err}')
        print("Root user enabled")


if __name__ == "__main__":
    main()