#!/usr/bin/env python

# Copies the vhd within the provided zip to all destination accounts specified in the provided config file

# When I tried it from scratch on a pc in the brisbane office and 4 destination accounts in 4 seperate regions
# around the world it completed in 5min 20 secs. Pretty damn good, if I do say so myself 😁.

import os
import sys
import subprocess
import argparse
import zipfile
import json
from subprocess import CompletedProcess
from typing import List


def extract_vhd(zip_file_path: str, vhd_output_path, is_dev_zip: bool) -> str:
    """Extract the vhd locally and write it to the specifed path

    I want to use tmpfs but this might be too large for dev images so i'll write it to the provided filepath

    Args:
        zip_file_path (str): [description]
    """

    if not os.path.isfile(zip_file_path):
        raise FileNotFoundError(f"Failed to locate zip '{zip_file_path}'. It doesn't exist or isn't a zip")

    if os.path.exists(vhd_output_path):
        # These are big files, lets not do more writing then we have to
        raise FileExistsError(f"Failed to extract vhd to '{vhd_output_path}'. File already exists")

    with zipfile.ZipFile(zip_file_path) as zip:
        path_in_zip = "Ironman/Ironman-azure.vhd" if not is_dev_zip else "Ironman-dev/Ironman-azure.vhd"
        vhd_info: zipfile.ZipInfo = zip.getinfo(path_in_zip)
        vhd_info.filename = vhd_output_path # Set the path that the vhd will be saved to
        zip.extract(vhd_info)


def copy_from_localhost_to_intermediate_acct(vhd_path: str, intermediate_acct: dict):
    """Copy the vhd from the localhost to the intermediate account

    Args:
        vhd_path (str): [description]
        intermediate_acct (dict): [description]
    """

    destination_uri = f"https://{intermediate_acct['name']}.blob.core.windows.net/{intermediate_acct['writeContainer']}/{intermediate_acct['sas']}"

    cmd = [
            "azcopy", "copy",
            vhd_path,
            destination_uri, # same deal here
            "--overwrite", "false", # only copy the file if a file with the same name doesn't already exist at the destination
            "--blob-type", "PageBlob" # Azcopy should be smart enough to deduce that since .vhd is the extension it'll write a page blob but i dont trust it
        ]

        # azcopy copy ./Ironman-azure.vhd "https://xmstorageacct.blob.core.windows.net/davidc-lh/?sv=2019-12-12&ss=bfqt&srt=sco&sp=rwdlacupx&se=2021-01-12T07:41:42Z&st=2021-01-11T23:41:42Z&spr=https&sig=ZoGmhuh9QqflbMm9pQtvMPekhXIuyGN13AID5%2By7ERc%3D"

    print("local to intermediate cmd is: ", " ".join(cmd))

    result: CompletedProcess = subprocess.run(cmd, capture_output=False)

    exit_code = result.returncode

    if exit_code != 0:
        raise ChildProcessError(f"Failed to azcopy from localhost to intermediate container '{intermediate_acct['name']}'")


def delete_localhost_vhd(vhd_path: str):
    """Deletes the extracted vhd

    Args:
        vhd_path (str): path to it
    """

    os.remove(vhd_path)


def copy_from_intermediate_to_destination_accts(src: dict, destinations: List[dict], vhd_name: str):
    """Copy the vhd from the the intermediate storage account to all destination accounts.
    The copies are all synchronous since i've tried async and it doesn't work (check commit history for that).

    Args:
        src (dict): [description]
        destinations (List[dict]): [description]
        vhd_name (str): [description]
    """
    
    src_string = f"https://{src['name']}.blob.core.windows.net/{src['writeContainer']}/{vhd_name}{src['sas']}"

    for dst in destinations:
        dst_string = f"https://{dst['name']}.blob.core.windows.net/{dst['writeContainer']}/{vhd_name}{dst['sas']}"

        cmd = [
            "azcopy", "copy",
            f"{src_string}", # azcopy isn't happy unless we add '' around this string when using on cmdline but it is fine here
            f"{dst_string}", # same deal here
            "--recursive", # Even though i am only copying one file this is needed due to a bug in azcopy
            "--overwrite", "false", # only copy the file if a file with the same name doesn't already exist at the destination
            "--blob-type", "PageBlob"
        ]

        # print("cmd is: ", cmd) # debugger helper
        
        result: CompletedProcess = subprocess.run(cmd, capture_output=False)

        exit_code = result.returncode

        if exit_code != 0:
            raise ChildProcessError(f"Failed to azcopy from intermediate to destination container '{dst['name']}'")


def main():

    parser = argparse.ArgumentParser(description="""
Extract the vhd file out of the ironman zip, upload it to southeast aus region (for fastest upload) from there, copy it to the storage accounts of the regions that MS will use. This leverages the speed of the Azure backbone network.
If this fails please triple-double check that the SAS's of your storage accounts are correct i.e. correct access level, not expired, etc
    """)

    parser.add_argument("-a", "--storage-accounts", metavar="storage_accounts", type=str, required=True, help="Specifies storage account params")
    parser.add_argument("-z", "--zip", metavar="zip", type=str, required=True, help="Specifies the zip to extract the vhd from and upload. The name of the vhd is the same as the zip but with .zip changed to .vhd")
    parser.add_argument("-d", "--dev", dest="is_dev_zip", action="store_true", help="Whether the supplied zip contains a dev image")
    args = parser.parse_args()

    storage_accounts_file_path = args.storage_accounts
    zip_file_path = args.zip
    is_dev_zip = args.is_dev_zip

    if not os.path.isfile(storage_accounts_file_path):
        sys.exit(f"Storage accounts file '{storage_accounts_file_path}' does not exist or is not a file")
    
    print(f"Loading destinations in from '{storage_accounts_file_path}'")
    with open(storage_accounts_file_path, "r") as storage_accounts_file:
        storage_accounts_file_contents: dict = json.load(storage_accounts_file)
    
    # do a quick bit of validation
    assert len(storage_accounts_file_contents["intermediateStorageAccount"]) == 3, "invalid 'intermediateStorageAccount'"
    assert isinstance(storage_accounts_file_contents["destinationStorageAccounts"], list), "invalid 'destinationStorageAccounts'"
    assert all(isinstance(x, dict) and len(x) == 3 for x in storage_accounts_file_contents["destinationStorageAccounts"]), "invalid 'destinationStorageAccounts' children"

    intermediate_acct = storage_accounts_file_contents["intermediateStorageAccount"]
    destination_accts: List[dict] = storage_accounts_file_contents["destinationStorageAccounts"]

    print(f"Loaded {len(destination_accts)}")

    if len(destination_accts) == 0:
        print("No destinations to copy to.")
        sys.exit(0)


    # Extract the .vhd out of the zip to the current directory
    zip_basename: str = os.path.basename(zip_file_path)
    vhd_basename = zip_basename.replace(".zip", ".vhd", 1)
    vhd_path = f"{vhd_basename}" # Write to the current directory
    print("Extracting vhd out of zip (this will take a couple of minutes) ...")
    extract_vhd(zip_file_path, vhd_path, is_dev_zip)
    print("Extract complete")

    print("Uploading to intermediate storage account...")
    copy_from_localhost_to_intermediate_acct(vhd_path, intermediate_acct)
    print("Finished upload to intermediate storage account")

    print("Deleting local extracted vhd...")
    delete_localhost_vhd(vhd_path)
    print("Localhost vhd deleted")

    print("Uploading to destination storage accounts...")
    copy_from_intermediate_to_destination_accts(intermediate_acct, destination_accts, vhd_basename)
    print("Finished upload to destination storage account")

    print("All done")


if __name__ == "__main__":
    main()