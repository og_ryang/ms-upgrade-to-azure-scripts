#!/usr/bin/env python
import argparse
import subprocess

from firewall import (
    FirewallPort,
    FIREWALL_PUBLIC_IPV4,
)
from utilities.common import (
    az_login,
    get_script_dir,
    get_vnet_peers_script_args,
    VALID_MS_LHS,
)


def create(vhd_name: str):
    # Make sure that the Azure CLI has a valid login before running everything
    # else
    az_login()

    # Get the current script dir
    script_dir = get_script_dir()
    vnet_peers_commaed, vnet_peers_spaced = get_vnet_peers_script_args()

    # Spin up LHs
    print("\n\nCreating lighthouses")
    rc = subprocess.run(f"parallel {script_dir}/create_vm.py -v {vhd_name} -ms_lh {{}} ::: {vnet_peers_spaced}",
                        shell=True, text=True)
    if rc.returncode != 0:
        print("Failed to create lighthouses")
        return 1

    # Spin up external services
    print("\n\nCreating external services")
    rc = subprocess.run([f"{script_dir}/external_services.py", "--create"], text=True)
    if rc.returncode != 0:
        print("Failed to create external services")
        return 1

    return 0


def delete():
    # Make sure that the Azure CLI has a valid login before running everything
    # else
    az_login()

    # Get the current script dir
    script_dir = get_script_dir()
    vnet_peers_commaed, vnet_peers_spaced = get_vnet_peers_script_args()

    # Kill external services
    print("\n\nDestroying external services")
    rc = subprocess.run([f"{script_dir}/external_services.py", "--delete"], text=True)
    if rc.returncode != 0:
        print("Failed to destroy external services")
        return 1

    # Destroy LHs
    print("\n\nDestroying lighthouses")
    rc = subprocess.run(f"parallel {script_dir}/delete_vm.py -ms_lh {{}} ::: {vnet_peers_spaced}",
                        shell=True, text=True)
    if rc.returncode != 0:
        print("Failed to create lighthouses")
        return 1

    return 0


def main():
    parser = argparse.ArgumentParser(description="""
Spin up the Public IP LHs 'Mockrosoft' scenario.\n"

"""
    )

    # Make --create, --delete flags mutually exclusive
    create_delete_group = parser.add_mutually_exclusive_group(required=True)
    create_delete_group.add_argument("--create", action="store_true")
    create_delete_group.add_argument("--delete", action="store_true")
    parser.add_argument("-v", "--vhd", type=str, default=None, dest="vhd_name",
                        help="The name of the vhd within the blob container")
    args = parser.parse_args()

    if args.create and args.vhd_name:
        return create(args.vhd_name)
    elif args.delete:
        return delete()
    else:
        # Tried to run --create with no -v option
        print("A VHD must be specified for the --create option")
        return 1


if __name__ == "__main__":
    main()