#!/usr/bin/env python

"""
Launches 5 containers all in their own Azure Container Instance (ACI) since az CLI and
docker CLI spin up single containers per ACI (https://docs.docker.com/cloud/aci-container-features/)
Compose spins up multiple containers per ACI but thats not used in this script.
"""

import time
import sys
import argparse
from typing import List
from tqdm import tqdm
from tabulate import tabulate
from firewall import FIREWALL_PRIVATE_IPV4, HUB_VNET_NAME
from utilities.common import VMResourceNames, az_cli, RESOURCE_GROUP

EXTERNAL_SERVICES_VNET_NAME = "msmigrate-services-vnet"
EXTERNAL_SERVICES_VNET_ADDR_PREFIX = "10.0.5.0/24"
EXTERNAL_SERVICES_SUBNET_NAME = "subnet-services"
EXTERNAL_SERVICES_SUBNET_ADDR_PREFIX = EXTERNAL_SERVICES_VNET_ADDR_PREFIX

EXTERNAL_SERVICE_NAME_RSYSLOG = "rsyslog"
EXTERNAL_SERVICE_NAME_SNMPTRAPD = "snmptrapd"
EXTERNAL_SERVICE_NAME_TAC_PLUS_1 = "tacplus-1"
EXTERNAL_SERVICE_NAME_TAC_PLUS_2 = "tacplus-2"
EXTERNAL_SERVICE_NAME_NTPD = "ntpd"
EXTERNAL_SERVICES = [
    EXTERNAL_SERVICE_NAME_RSYSLOG,
    EXTERNAL_SERVICE_NAME_SNMPTRAPD,
    EXTERNAL_SERVICE_NAME_TAC_PLUS_1,
    EXTERNAL_SERVICE_NAME_TAC_PLUS_2,
    EXTERNAL_SERVICE_NAME_NTPD
]

LOCATION = "westus2" # This is the same as quincy


def get_container_group_name(service_name: str):
    if service_name not in EXTERNAL_SERVICES:
        raise ValueError(f"service name '{service_name}' is invalid. Valid values are {EXTERNAL_SERVICES}")
    return f"msmigrate-services-{service_name}"


def create_vnet():
    vnet_name = EXTERNAL_SERVICES_VNET_NAME
    vnet_subnet_prefixes = EXTERNAL_SERVICES_VNET_ADDR_PREFIX
    location = LOCATION
    subnet_name = EXTERNAL_SERVICES_SUBNET_NAME

    was_cmd_successful, result, _ = az_cli([
        "network", "vnet", "create",
        "--resource-group", RESOURCE_GROUP,
        "--name", vnet_name,
        "--ddos-protection", "false",
        "--address-prefixes", vnet_subnet_prefixes,
        "--location", location,
        "--subnet-name", subnet_name,
        "--subnet-prefixes", vnet_subnet_prefixes,
        "--tags", "team=xm", f"purpose=msmigrate-services"
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create VNet '{vnet_name}'. Error: {result}")


def create_rsyslog(no_public_ip: bool) -> str:
    """
    """
    service_name = EXTERNAL_SERVICE_NAME_RSYSLOG
    container_group_name = get_container_group_name(service_name)     # There will only be one container in here
    image = "docker.io/ryangwaite/rsyslog:latest"

    args = [
        "container", "create",
        "--location", LOCATION,
        "--resource-group", RESOURCE_GROUP,
        "--name", container_group_name,
        "--image", image,
        "--ports", "514",
        "--protocol", "UDP",
    ]

    if no_public_ip:
        # Attach it to the services VNet
        args += [
            "--ip-address", "Private",
            "--vnet", EXTERNAL_SERVICES_VNET_NAME,
            "--subnet", EXTERNAL_SERVICES_SUBNET_NAME,
        ]
    else:
        # Put it on the public internet without VNet
        args += [
            "--ip-address", "Public",
        ]

    # Theres no way to set a network profile name using the azure cli. See https://github.com/Azure/azure-cli/issues/13191
    # We'll query this from the container group proir to deletion as part of the delete flow

    was_cmd_successful, result, exit_code = az_cli(args)
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create {service_name} service: {result}")


def create_snmptrapd(no_public_ip: bool):
    """Creates the snmptrapd container
    """
    
    service_name = EXTERNAL_SERVICE_NAME_SNMPTRAPD
    container_group_name = get_container_group_name(service_name)     # There will only be one container in here
    image = "docker.io/ryangwaite/snmptrapd"

    args = [
        "container", "create",
        "--location", LOCATION,
        "--resource-group", RESOURCE_GROUP,
        "--name", container_group_name,
        "--image", image,
        "--ports", "162",
        "--protocol", "UDP",
    ]

    if no_public_ip:
        # Attach it to the services VNet
        args += [
            "--ip-address", "Private",
            "--vnet", EXTERNAL_SERVICES_VNET_NAME,
            "--subnet", EXTERNAL_SERVICES_SUBNET_NAME,
        ]
    else:
        # Put it on the public internet without VNet
        args += [
            "--ip-address", "Public",
        ]

    was_cmd_successful, result, _ = az_cli(args)
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create {service_name} container. There is an 'az' CLI bug here so maybe thats it. Error: {result}")


def create_ntpd(no_public_ip: bool):
    """Creates the ntpd container
    """
    service_name = EXTERNAL_SERVICE_NAME_NTPD
    container_group_name = get_container_group_name(service_name)     # There will only be one container in here
    image = "docker.io/ryangwaite/ntpd"

    args = [
        "container", "create",
        "--location", LOCATION,
        "--resource-group", RESOURCE_GROUP,
        "--name", container_group_name,
        "--image", image,
        "--ports", "123",
        "--protocol", "UDP"
    ]

    if no_public_ip:
        # Attach it to the services VNet
        args += [
            "--ip-address", "Private",
            "--vnet", EXTERNAL_SERVICES_VNET_NAME,
            "--subnet", EXTERNAL_SERVICES_SUBNET_NAME,
        ]
    else:
        # Put it on the public internet without VNet
        args += [
            "--ip-address", "Public",
        ]

    was_cmd_successful, result, _ = az_cli(args)
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create {service_name} container. There is an 'az' CLI bug here so maybe thats it. Error: {result}")


def create_tac_plus(no_public_ip: bool, instance_num: int):
    """Create and start the tac_plus container with the tac_plus.conf
    corresponding to `instance_num`

    PREREQUISITES:
        - Create a storage account in the same region that these containers are deployed
        - Within it, create a file-share with the name "services-tacplus-`instance_num`".
        - In the file-share, upload tac_plus.conf
        - Set the storage access key below for the storage account
    """

    if instance_num not in [1, 2]:
        raise ValueError(f"invalid instance num '{instance_num}' for tac_plus container")

    service_name = EXTERNAL_SERVICE_NAME_TAC_PLUS_1 if instance_num == 1 else EXTERNAL_SERVICE_NAME_TAC_PLUS_2
    container_group_name = get_container_group_name(service_name)     # There will only be one container in here
    image = "docker.io/ryangwaite/tac_plus"

    # Mount options
    storage_account_name = VMResourceNames.get_storage_acct_name("quincy")
    storage_account_key = "iA3tgPdZ5dXyVNOY5HNB/IBKX68d+Fab7ymRX9fizSXP0eHBzyEDZKc9sUKtSBDmbPwL+NKoqiK/E/42SqSZvQ=="
    file_share_name = f"services-tacplus-{instance_num}"
    container_mount_path = "/etc/tacacs+/"

    args = [
        "container", "create",
        "--location", LOCATION,
        "--resource-group", RESOURCE_GROUP,
        "--name", container_group_name,
        "--image", image,
        "--ports", "49",
        "--azure-file-volume-account-name", storage_account_name,
        "--azure-file-volume-account-key", storage_account_key,
        "--azure-file-volume-share-name", file_share_name,
        "--azure-file-volume-mount-path", container_mount_path,
    ]

    if no_public_ip:
        # Attach it to the services VNet
        args += [
            "--ip-address", "Private",
            "--vnet", EXTERNAL_SERVICES_VNET_NAME,
            "--subnet", EXTERNAL_SERVICES_SUBNET_NAME,
        ]
    else:
        # Put it on the public internet without VNet
        args += [
            "--ip-address", "Public",
        ]

    was_cmd_successful, result, _ = az_cli(args)
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create {service_name} container. There is an 'az' CLI bug here so maybe thats it. Error: {result}")


def delete_container(service_name: str):
    """Deletes a container gfoup which contains
    the single container identified by `service_name`

    Args:
        service_name (str): [description]

    Raises:
        EnvironmentError: [description]
    """
    container_group_name = get_container_group_name(service_name)
    was_cmd_successful, result, _ = az_cli([
        "container", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", container_group_name,
        "--yes" # Don't prompt for comfirmation
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to delete container group for {service_name}. Error: {result}")


def peer_vnet():
    """Peer the hub vnet to the services VNet
    We need to run the command twice each time with the vnets reversed

    Raises:
        EnvironmentError: [description]
    """

    hub_vnet_name = HUB_VNET_NAME
    services_vnet_name = EXTERNAL_SERVICES_VNET_NAME

    hub_to_services_peering_name = f"msmigrate-hub-to-services"
    services_to_hub_peering_name = f"msmigrate-services-to-hub"

    # 1. hub -> services
    was_cmd_successful, result, _ = az_cli([
        "network", "vnet", "peering", "create",
        "--resource-group", RESOURCE_GROUP,
        "--name", hub_to_services_peering_name,
        "--vnet-name", hub_vnet_name,
        "--remote-vnet", services_vnet_name,
        "--allow-forwarded-traffic",
        "--allow-vnet-access",
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create VNET peering '{hub_to_services_peering_name}'. Error: {result}")

    # 2. services -> hub
    was_cmd_successful, result, _ = az_cli([
        "network", "vnet", "peering", "create",
        "--resource-group", RESOURCE_GROUP,
        "--name", services_to_hub_peering_name,
        "--vnet-name", services_vnet_name,
        "--remote-vnet", hub_vnet_name,
        "--allow-forwarded-traffic",
        "--allow-vnet-access",
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create VNET peering '{services_to_hub_peering_name}'. Error: {result}")


def remove_vnet_peering():
    """Deletes both side of the peering between hub and services Vnets

    Args:
        ms_lh (str): [description]
    """
    hub_vnet_name = HUB_VNET_NAME
    lh_vnet_name = EXTERNAL_SERVICES_VNET_NAME

    hub_to_services_peering_name = f"msmigrate-hub-to-services"
    services_to_hub_peering_name = f"msmigrate-services-to-hub"

    # 1. services -> hub
    successful, result, _ = az_cli([
        "network", "vnet", "peering", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", services_to_hub_peering_name,
        "--vnet-name", lh_vnet_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete VNet peering '{services_to_hub_peering_name}'. Error: {result}")

    # 2. hub -> services
    successful, result, _ = az_cli([
        "network", "vnet", "peering", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", hub_to_services_peering_name,
        "--vnet-name", hub_vnet_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete VNet peering '{hub_to_services_peering_name}'. Error: {result}")


def add_route_table_to_vnet_peer():
    """The external services cannot reach the LH's in the other VNets by default. This
    adds a route to the gateway that resides in the VNet to forward all traffic to the firewalls
    private IP where it then gets forwarded to the LH VNets.

    Args:
        ms_lh (str): [description]
    """

    location = LOCATION
    route_table_name = "msmigrate-services-vnet-route-table"
    vnet_name = EXTERNAL_SERVICES_VNET_NAME
    vnet_subnet_name = EXTERNAL_SERVICES_SUBNET_NAME
    firewall_private_ip = FIREWALL_PRIVATE_IPV4

    # 1. Create route table
    successful, result, exit_code = az_cli([
        "network", "route-table", "create",
        "--resource-group", RESOURCE_GROUP,
        "--location", location,
        "--name", route_table_name,
        "--disable-bgp-route-propagation", "true",
        "--tags", "team=xm", f"msmigrate-lh-services"
    ])
    if not successful:
        raise EnvironmentError(f"Failed to create route table '{route_table_name}'. Error: {result}")

    # 2. Add default route to firewall
    successful, result, exit_code = az_cli([
        "network", "route-table", "route", "create",
        "--resource-group", RESOURCE_GROUP,
        "--name", "default",
        "--route-table-name", route_table_name,
        "--address-prefix", "0.0.0.0/0", # default route
        "--next-hop-type", "VirtualAppliance",
        "--next-hop-ip-address", firewall_private_ip,
    ])
    if not successful:
        raise EnvironmentError(f"Failed to add rule to route table '{route_table_name}'. Error: {result}")

    # 3. Associate route-table with VNet subnet
    successful, result, exit_code = az_cli([
        "network", "vnet", "subnet", "update",
        "--resource-group", RESOURCE_GROUP,
        "--name", vnet_subnet_name,
        "--vnet-name", vnet_name,
        "--route-table", route_table_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to associate route table '{route_table_name}' with subnet '{vnet_subnet_name}'. Error: {result}")


def remove_route_table_from_vnet_peer():
    """Removes the route table containing routes that forwards via the firewall
    private IP
    """

    route_table_name = "msmigrate-services-vnet-route-table"
    vnet_name = EXTERNAL_SERVICES_VNET_NAME
    vnet_subnet_name = EXTERNAL_SERVICES_SUBNET_NAME

    # 1. Disassociate route-table from VNet subnet
    successful, result, _ = az_cli([
        "network", "vnet", "subnet", "update",
        "--resource-group", RESOURCE_GROUP,
        "--name", vnet_subnet_name,
        "--vnet-name", vnet_name,
        "--route-table", "" # dissacossiate
    ])
    if not successful:
        raise EnvironmentError(f"Failed to disassociate route table '{route_table_name}' from subnet '{vnet_subnet_name}'. Error: {result}")

    # 2. Delete route table
    successful, result, exit_code = az_cli([
        "network", "route-table", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", route_table_name,
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete route-table '{route_table_name}'. Error: {result}")


def create(no_public_ip: bool):

    try:
        print("Creating VNet...")
        create_vnet()
        print("VNet created successfully")
    except EnvironmentError as e:
        sys.exit(e)

    try:
        print("Creating rsyslogd...")

        create_rsyslog(no_public_ip)

        print("Finished creating rsyslogd. Creating snmptrapd container...")

        create_snmptrapd(no_public_ip)

        print("Finished creating snmptrapd. Creating ntpd container...")

        create_ntpd(no_public_ip)

        print("Finished creating ntpd. Creating tac_plus-1 container...")

        create_tac_plus(no_public_ip, 1)

        print("Finished creating tac_plus-1. Creating tac_plus-2 container...")

        create_tac_plus(no_public_ip, 2)

        print("Finished creating tac_plus-2")

    except EnvironmentError as e:
        sys.exit(e)

    if no_public_ip:
        print("Peering vnet with hub...")
        try:
            peer_vnet()
        except EnvironmentError as err:
            sys.exit(f"Failed to add VNET peer to hub: {err}")

        print("Add route table to hub...")

        try:
            add_route_table_to_vnet_peer()
        except EnvironmentError as err:
            sys.exit(f"Failed to add route table to external services VNet: {err}")

    summary_table = []

    # Get the IP address of all containers
    for service in EXTERNAL_SERVICES:
        was_cmd_successful, result, _ = az_cli([
            "container", "show",
            "--resource-group", RESOURCE_GROUP,
            "--name", get_container_group_name(service),
        ])
        if not was_cmd_successful:
            raise EnvironmentError(f"Failed to query ip address of {service} container. Error: {result}")

        summary_table.append([
            service,
            result["ipAddress"]["type"],
            result["ipAddress"]["ip"],
            str(result["ipAddress"]["ports"]),
        ])

    print(tabulate(summary_table, headers=["Service", "Type", "IP Address", "Exposed Ports"]))

    print("\nAttach to container: az container attach --resource-group xm-rg --name msmigrate-services-<service>")
    print("Get container logs: az container logs --resource-group xm-rg --name msmigrate-services-<service>")


def delete(no_public_ip: bool):

    if no_public_ip:

        try:
            remove_route_table_from_vnet_peer()
        except EnvironmentError as e:
            sys.exit(f"Failed to remove route table from vnet peer: {e}")

        try:
            remove_vnet_peering()
        except EnvironmentError as e:
            sys.exit(f"Failed to remove VNet peering: {e}")

        # This resource is only created for private deployments
        print("Querying network profile...")

        # Get network profile of one they all share the same one
        container_group = get_container_group_name(EXTERNAL_SERVICE_NAME_RSYSLOG)
        was_cmd_successful, result, _ = az_cli([
            "container", "show",
            "--resource-group", RESOURCE_GROUP,
            "--name", container_group
        ])
        if was_cmd_successful:
            network_profile_id: str = result["networkProfile"]["id"] # e.g. '/subscriptions/e0b23358-9323-4c4b-a408-8be0221fc1a8/resourceGroups/xm-rg/providers/Microsoft.Network/networkProfiles/aci-network-profile-msmigrate-services-vnet-subnet-services'
        else:
            sys.exit(f"Failed to query container group '{container_group}' for network profile ID. Error: {result}")

        print(network_profile_id)

    print("Deleting containers...")

    for service_name in tqdm(EXTERNAL_SERVICES):
        try:
            delete_container(service_name)
        except EnvironmentError as e:
            sys.exit(e)

    print("Containers deleted")

    if no_public_ip:
        # Delete the extra resources created in private deployments

        wait_secs = 60
        print(f"Waiting {wait_secs}s for the containers to stop...")
        time.sleep(wait_secs)

        print("Deleting network profile...")
        
        # Some useful commands in case this fails:
        #   - Get the name with: az network profile list --resource-group xm-rg
        #   - Delete with: az network profile delete --resource-group xm-rg --name <paste name from above> -y
        # Delete the network profile
        was_cmd_successful, result, _ = az_cli([
            "network", "profile", "delete",
            "--id", network_profile_id,
            "--yes" # Don't prompt for comfirmation
        ])
        if not was_cmd_successful:
            raise EnvironmentError(f"Failed to delete network profile. Error: {result}")

        print("Network Profile deleted. Deleting VNet...")

        # Delete the VNet
        was_cmd_successful, result, _ = az_cli([
            "network", "vnet", "delete",
            "--resource-group", RESOURCE_GROUP,
            "--name", EXTERNAL_SERVICES_VNET_NAME,
        ])
        if not was_cmd_successful:
            raise EnvironmentError(f"Failed to services VNet. Error: {result}")

        print("VNet deleted")

    print("Done")

def main():
    parser = argparse.ArgumentParser(description="""
Create/delete the external services of 2x tacplus servers, 1x NTP, 1x snmptrapd, 1x rsyslogd in Azure.\n"

""")

    # Make --create, --delete flags mutually exclusive
    create_delete_group = parser.add_mutually_exclusive_group(required=True)
    create_delete_group.add_argument("--create", action="store_true")
    create_delete_group.add_argument("--delete", action="store_true")
    parser.add_argument("--no-public-ip", default=False, dest="no_public_ip", action="store_true",
            help="If provided, the external services boot with private IPv4 addresses only, else public only")
    args = parser.parse_args()

    no_public_ip = args.no_public_ip

    if args.create:
        create(no_public_ip)
    elif args.delete:
        delete(no_public_ip)
    else:
        # I dunno if we can ever reach this
        raise EnvironmentError("How did this even happen?")


if __name__ == "__main__":
    main()