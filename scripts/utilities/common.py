import argparse
import json
import os
import subprocess
import sys
from typing import Union, Tuple, Any


# Xenomorph's only have access to this group so we'll hardcode this
RESOURCE_GROUP = "xm-rg"

# These are the only supported ms lh names
VALID_MS_LHS = ["quincy", "cheyenne", "dublin", "singapore"]


def az_cli(args: Union[list, str]) -> Tuple[bool, Any]:
    """Runs the command

    Args:
        args ([type]): [description]

    Raises:
        cli.result.error: [description]

    Returns:
        tuple[bool, Any]: [description]
    """
    args = args.split() if isinstance(args,str) else args

    # add the leading "az" if missing
    if args[0] != "az":
        args = ["az"] + args

    # The output should always be json
    args += ["--output", "json"]

    # All list items need to be strings
    args = [str(i) for i in args]

    result: subprocess.CompletedProcess = subprocess.run(args, capture_output=True)
    exit_code = result.returncode
    if exit_code != 0:
        return (False, result.stderr.decode("utf-8"), exit_code)

    decoded_stdout = result.stdout.decode("utf-8")
    try:
        jsonified_stdout = json.loads(decoded_stdout)
        return (True, jsonified_stdout, exit_code)
    except json.JSONDecodeError as error:
        # Despite always supplying output as json some successful commands (e.g. vm stop ...) don't return
        # JSON. These seem to be rare. Rather than keeping track of which commands don't
        # return JSON, i'll try it (as above) then if it fails, return the raw string.
        return (True, decoded_stdout, exit_code)


def az_login():
    """
    Ensure the user is logged in. If they are not, try to make them login.
    """
    try:
        was_cmd_successful, account_details, _ = az_cli(["account", "show"])
        if not was_cmd_successful or ("state" in account_details and account_details["state"] != "Enabled"):
            raise Exception(f"Not logged in. Response from `az account show`:\n{account}")
    except Exception:
        if sys.stdout.isatty():
            print("Please login to Azure in your browser.")
            az_cli(["login"])
        else:
            raise


def get_azure_region(ms_lh: str) -> str:
    """Maps the ms_lh to an azure region string

    Aside: You can list all azure regions with the following:

        $ az account list-locations -o table

    A str "name" column is what we are returning.

    Args:
        ms_lh (str): [description]

    Returns:
        str: [description]
    """
    az_region_map = {
        "quincy": "westus2",
        "cheyenne": "westcentralus",
        "dublin": "westeurope",
        "singapore": "southeastasia",
    }

    if ms_lh not in az_region_map:
        raise ValueError(f"The specified lh '{ms_lh}' is invalid")
    return az_region_map[ms_lh]


def get_common_cli_argparser(description="") -> argparse.ArgumentParser:
    """Get an instance of the argparse ArgumentParserwith the common
    options already present

    Args:
        description (str, optional): [description]. Defaults to "".

    Returns:
        [type]: [description]
    """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-ms_lh", type=str, choices=VALID_MS_LHS, required=True,
            help="The LH to create a vm for. The script matches this to an Azure region to create it in.")

    return parser


def get_script_dir():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


def get_static_private_ipv4(ms_lh: str) -> str:
    """Get the static private ip of the lighthouse for use in its VNET

    Args:
        ms_lh (str): [description]

    Returns:
        str: [description]
    """

    az_ipv4_map = {
        "quincy": "10.0.1.4",
        "cheyenne": "10.0.2.4",
        "dublin": "10.0.3.4",
        "singapore": "10.0.4.4",
    }

    if ms_lh not in az_ipv4_map:
        raise ValueError(f"The specified lh '{ms_lh}' is invalid")
    return az_ipv4_map[ms_lh]


def get_static_public_ipv4(ms_lh: str) -> str:
    """Get the static public ip of the lighthouse for use on the public
    internet

    Args:
        ms_lh (str): [description]

    Returns:
        str: [description]
    """

    az_ipv4_map = {
        "quincy": "52.149.7.186",
        "cheyenne": "13.78.164.166",
        "dublin": "65.52.138.202",
        "singapore": "40.65.160.251",
    }

    if ms_lh not in az_ipv4_map:
        raise ValueError(f"The specified lh '{ms_lh}' is invalid")
    return az_ipv4_map[ms_lh]


def get_vnet_peers_script_args():
    vnet_peers_commaed = ""
    vnet_peers_spaced = ""
    for valid_lhs in VALID_MS_LHS:
        vnet_peers_commaed += f"{valid_lhs},"
        vnet_peers_spaced += f"{valid_lhs} "
    vnet_peers_commaed = vnet_peers_commaed[:-1]

    return vnet_peers_commaed, vnet_peers_spaced


class VMResourceNames:
    """All the vm resource names are in here so they can be changed from one place
    """
    def get_img_name(ms_lh: str):
        return f"lh-20q3.2-image-{ms_lh}"

    def get_vm_name(ms_lh: str):
        return f"lh-20q3.2-vm-{ms_lh}"

    def get_vnet_name(ms_lh: str):
        return f"msmigrate-lh-vnet-{ms_lh}"

    def get_subnet_name(ms_lh: str):
        return f"subnet-{ms_lh}"

    def get_public_ipv4_name(ms_lh: str):
        return f"lh-20q3.2-vm-{ms_lh}-ipv4"

    def get_nsg_name(ms_lh: str):
        return f"msmigrate-lh-nsg-{ms_lh}"

    def get_storage_acct_name(ms_lh: str):
        # Hit a character limit with singapore so had to do this :(
        return f"xmstorageaccount{ms_lh}" if ms_lh != "singapore" else "xmstorageaccountsgp"

    def get_route_table_name(ms_lh: str):
        return f"msmigrate-{ms_lh}-vnet-route-table"

    