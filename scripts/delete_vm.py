#!/usr/bin/env python

import sys
import argparse
from typing import List, Dict, Any, Iterable

import utilities.common as common
from utilities.common import az_cli
from utilities.common import VMResourceNames


def stop_and_deallocate_vm(vm_name: str, statuses: List[Dict[str,Any]]):
    """Transitions the vm to the deallocated and stopped state.

    Note: a VM must be both stopped and deallocated for us to stop paying for it

    Args:
        vm_name (str): [description]
        statuses (List[Dict[str,Any]]): Each item contains an entry that looks like these here: https://docs.microsoft.com/en-us/azure/virtual-machines/states-lifecycle#power-states
        There are other "code" values in here but we are only interested in the ones that lead with "PowerState/"
    """

    # Need to find the current powerstate. The code of interest looks like PowerState/<extract this state>
    powerstate_statuses: List = list(filter(lambda x: x["code"].startswith("PowerState/"), statuses))
    powerstate: str = powerstate_statuses[0]["code"].split("/")[-1] # There should only be one powerstate and we want the code on the right of the slash

    # Based on the state machine here: https://docs.microsoft.com/en-us/azure/virtual-machines/states-lifecycle#power-states
    # It looks like it is safe to call deallocate from either the 'stopped' or 'running' states. 'starting', 'stopping' and 'running'
    # states are all temporary transition states so i'll avoid deallocating in these cases. If its already in the 'deallocated
    # state theres nothing to do.

    if powerstate in ["starting", "stopping", "deallocating"]:
        raise EnvironmentError(f"'{vm_name}' is currently in transitioning state '{powerstate}'. Please wait for it to settle before trying again.")

    if powerstate == "deallocated":
        # We are already in the final state - nothing to do here
        return

    # Deallocate the VM (which stops it too)
    was_cmd_successful, result, _ = az_cli([
        "vm", "deallocate",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", vm_name
    ])
    if not was_cmd_successful:
        raise EnvironmentError("Failed to deallocate VM '{vm_name}'. Error: {result}")


def delete_vm_image(vm_img_name: str):
    """Deletes the image that was used in the VM

    Args:
        vm_img_name (str): [description]

    Raises:
        EnvironmentError: [description]
    """

    successful, result, exit_code = az_cli([
        "image", "delete",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", vm_img_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete vm image '{vm_img_name}'. Error: {result}")


def delete_vm_nic(vm_nic_res_id: str):
    """Deletes the NIC pointed to by the resource id

    The NIC cannot be in use when running this else an exception will be raised

    Args:
        vm_nic_res_id (str): id of the nic to delete

    Raises:
        EnvironmentError: [description]
    """
    successful, result, exit_code = az_cli([
        "network", "nic", "delete",
        "--resource-group", common.RESOURCE_GROUP,
        "--ids", vm_nic_res_id,
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete NIC. Error: {result}")


def delete_vm_os_disk(vm_os_disk_res_id: str):
    """Deletes the disk pointed to by provided resource id

    The disk cannot be in use when running this else an exception will be raised

    Args:
        vm_os_disk_res_id (str): id of the disk to delete

    Raises:
        EnvironmentError: [description]
    """
    successful, result, exit_code = az_cli([
        "disk", "delete",
        "--resource-group", common.RESOURCE_GROUP,
        "--ids", vm_os_disk_res_id,
        "--yes" # Don't prompt for confirmation
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete disk. Error: {result}")


def main():
    parser = common.get_common_cli_argparser(description="""
Delete one of <quincy|cheyenne|dublin|singapore> VM's in Azure.\n"

The only additional Azure resources to be deleted are ... to be decided
"""
    )

    args = parser.parse_args()

    # Check that this vm doesn't currently exist
    ms_lh = args.ms_lh
    ms_lh_vm_name = VMResourceNames.get_vm_name(ms_lh)
    print(f"Checking if VM '{ms_lh_vm_name}' currently exists...")
    was_cmd_successful, result, _ = az_cli([
        "vm", "get-instance-view",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", ms_lh_vm_name
    ])
    if not was_cmd_successful:
        sys.exit(f"VM '{ms_lh_vm_name}' doesn't exist. Leaving all existing resources as is.")

    # Extract a few details needed for cleanup later on
    vm_statuses: List[Dict[str,Any]] = result['instanceView']['statuses']
    vm_os_disk_res_id: str = result['storageProfile']['osDisk']['managedDisk']['id']
    vm_nic_res_id: str = result['networkProfile']['networkInterfaces'][0]['id']

    print(f"'{ms_lh_vm_name}' exists. Stopping and deallocating it...")
    try:
        stop_and_deallocate_vm(ms_lh_vm_name, vm_statuses)
    except EnvironmentError as error:
        sys.exit(f"Failed to stop and deallocate'{ms_lh_vm_name}'. Error: {error}")

    print(f"Deallocated '{ms_lh_vm_name}'. Deleting it...")
    was_cmd_successful, result, exit_code = az_cli([
        "vm", "delete",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", ms_lh_vm_name,
        "--yes" # Don't prompt for confirmation
    ])
    if not was_cmd_successful:
        sys.exit(f"Failed to deallocate '{ms_lh_vm_name}'. Error: {result}")

    print(f"Successfully deleted '{ms_lh_vm_name}'")

    img_name = VMResourceNames.get_img_name(ms_lh)
    print(f"Deleting image '{img_name}'...")
    try:
        delete_vm_image(img_name)
    except EnvironmentError as error:
        sys.exit(f"Failed to delete image with name '{img_name}' that was used in '{ms_lh_vm_name}'. Error: {error}")

    print(f"Successfully deleted VM image. Deleting os-disk and NIC dependent resources...")

    try:
        delete_vm_os_disk(vm_os_disk_res_id)
    except EnvironmentError as error:
        sys.exit(f"Failed to delete disk with id '{vm_os_disk_res_id}' that was used in '{ms_lh_vm_name}'. Error: {error}")

    try:
        delete_vm_nic(vm_nic_res_id)
    except EnvironmentError as error:
        sys.exit(f"Failed to NIC with id '{vm_nic_res_id}' that was used in '{ms_lh_vm_name}'. Error: {error}")

    print("Done")


if __name__ == "__main__":
    main()