import sys
import argparse
from dataclasses import dataclass

import utilities.common as common
from utilities.common import az_cli, get_azure_region
from utilities.common import VMResourceNames

@dataclass
class PortRule:
    name: str
    priority: int
    protocol: str               # Udp or Tcp
    src_addr_prefix: str = "*"  # Space-separated list of CIDR prefixes or IP ranges or '*' to match all IPs
    src_port_range: str = "*"   # e.g. 1000-1010 or * for all
    dst_addr_prefix: str = "*"  # Space-separated list of CIDR prefixes or IP ranges or '*' to match all IPs
    dst_port_range: str = "*"   # e.g. 1000-1010 or * for all
    access: str = "Allow"       # or "Deny"


def add_inbound_port_rules(nsg_name: str):

    """Adds port rules according to the table below.
    A priority is required so i'll give the SSH rule priority 1000
    and it increments with each subsequent rule.
    """

    rules = [
        PortRule(name="SSH", dst_port_range=22, protocol="Tcp", priority=1000),
        PortRule(name="HTTPS", dst_port_range=443, protocol="Tcp", priority=1001),
        PortRule(name="NodeVPN", dst_port_range=1194, protocol="Udp", priority=1002),
        PortRule(name="MIVPN", dst_port_range=1195, protocol="Udp", priority=1003),
    ]

    for rule in rules:
        print(f"Adding port rule '{rule.name}'...", end="", flush=True)
        was_cmd_successful, result, _ = az_cli([
            "network", "nsg", "rule", "create",
            "--resource-group", common.RESOURCE_GROUP,
            "--nsg-name", nsg_name,
            "--name", rule.name,
            "--priority", rule.priority,
            "--protocol", rule.protocol,
            "--source-address-prefixes", rule.src_addr_prefix,
            "--source-port-ranges", rule.src_port_range,
            "--destination-address-prefixes", rule.dst_addr_prefix,
            "--destination-port-ranges", rule.dst_port_range,
            "--access", rule.access
        ])
        if was_cmd_successful:
            print("Done")
        else:
            raise EnvironmentError(f"Failed to add inbound port rule '{rule.name}'. Error: {result}")


def main():

    parser = common.get_common_cli_argparser(description="""
Create a network security group for one of the <quincy|cheyenne|dublin|singapore> VM's in Azure.

The security group has the following inbound rules:

+----------+---------+------+----------+--------+-------------+--------+
| Priority |   Name  | Port | Protocol | Source | Destination | Action |
+----------+---------+------+----------+--------+-------------+--------+
| anything | SSH     | 22   | TCP      | Any    | Any         | Allow  |
+----------+---------+------+----------+--------+-------------+--------+
| anything | HTTPS   | 443  | TCP      | Any    | Any         | Allow  |
+----------+---------+------+----------+--------+-------------+--------+
| anything | NodeVPN | 1194 | UDP      | Any    | Any         | Allow  |
+----------+---------+------+----------+--------+-------------+--------+
| anything | MIVPN   | 1195 | UDP      | Any    | Any         | Allow  |
+----------+---------+------+----------+--------+-------------+--------+

This is sufficient to allow MI and node connections to and from the LH as well as HTTP and SSH

"""
    )

    # The default formatter was messing up my table in the description because it wraps at 80 chars
    parser.formatter_class = argparse.RawTextHelpFormatter

    args = parser.parse_args()

    # Check that this network security group doesn't currently exist
    ms_lh = args.ms_lh
    ms_lh_nsg_name = nsg_name = VMResourceNames.get_nsg_name(ms_lh)
    print(f"Checking if NSG '{ms_lh_nsg_name}' currently exists...")
    was_cmd_successful, *_ = az_cli([
        "network", "nsg", "show",
        "--resource-group", common.RESOURCE_GROUP,
        "--name", ms_lh_nsg_name
    ])
    if was_cmd_successful:
        sys.exit(f"Network Security Group '{ms_lh_nsg_name}' already exists. Leaving config unchanged")

    print(f"NSG '{ms_lh_nsg_name}' is new. Creating network security group...")

    successful, result, _ = az_cli([
        "network", "nsg", "create",
        "--resource-group", common.RESOURCE_GROUP,
        "--location", common.get_azure_region(ms_lh),
        "--name", ms_lh_nsg_name,
        "--tags", "team=xm", f"purpose=msmigrate-lh-{ms_lh}"
    ])

    if not successful:
        raise EnvironmentError(f"Failed to create NSG '{ms_lh_nsg_name}'. Error: {result}")

    print(f"Successfully created network security group. Now adding inbound port rules...")

    try:
        add_inbound_port_rules(ms_lh_nsg_name)
    except EnvironmentError as error:
        sys.exit(f"Failed to add inbound port rules to nsg '{ms_lh_nsg_name}', you'll have to add them manually in the Azure portal. Error: {error}")

    print(f"Successfully added inbound port rules. NSG setup is complete")


if __name__ == "__main__":
    main()