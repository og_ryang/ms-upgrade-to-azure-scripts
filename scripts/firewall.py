#!/usr/bin/env python

import sys
import argparse
from typing import List

from tabulate import tabulate
from tqdm import tqdm
from dataclasses import dataclass
from utilities.common import az_cli, VMResourceNames, RESOURCE_GROUP, VALID_MS_LHS, get_azure_region

HUB_VNET_NAME = "msmigrate-hub-vnet"
HUB_VNET_ADDR_SPACE = "10.0.0.0/24"
HUB_FIREWALL_SUBNET_ADDR_SPACE = "10.0.0.0/25" # -> 10.0.0.0 - 10.0.0.127
LOCATION = "westus2" # This is the same as quincy
FIREWALL_NAME = "msmigrate-hub-firewall"
FIREWALL_IPCONFIG_NAME = "msmigrate-hub-firewall-ipconfig"
FIREWALL_PUBLIC_IP_IPV4_NAME = "msmigrate-hub-firewall-ipv4"
FIREWALL_NAT_RULE_COLLECTION_NAME = "msmigrate-hub-firewall-nat-rule-collection"
FIREWALL_NETWORK_RULE_COLLECTION_NAME = "msmigrate-hub-firewall-network-rule-collection"
FIREWALL_PUBLIC_IPV4 = "20.47.120.20"
FIREWALL_PRIVATE_IPV4 = "10.0.0.4"

class FirewallPort:

    firewall_ports = {
        "quincy": {
            "ssh": 11022,
            "https": 11443,
            "node-vpn": 11194,
            "mi-vpn": 11195,
            "enroll-only-api": 11843,
        },
        "cheyenne": {
            "ssh": 12022,
            "https": 12443,
            "node-vpn": 12194,
            "mi-vpn": 12195,
            "enroll-only-api": 12843,
        },
        "dublin": {
            "ssh": 13022,
            "https": 13443,
            "node-vpn": 13194,
            "mi-vpn": 13195,
            "enroll-only-api": 13843,
        },
        "singapore": {
            "ssh": 14022,
            "https": 14443,
            "node-vpn": 14194,
            "mi-vpn": 14195,
            "enroll-only-api": 14843,
        }
    }

    def _validate(ms_lh: str):
        if ms_lh not in VALID_MS_LHS:
            raise ValueError(f"The specified lh '{ms_lh}' is invalid")

    def ssh_for(ms_lh: str):
        __class__._validate(ms_lh)
        return __class__.firewall_ports[ms_lh]["ssh"]

    def https_for(ms_lh: str):
        __class__._validate(ms_lh)
        return __class__.firewall_ports[ms_lh]["https"]

    def node_vpn_for(ms_lh: str):
        __class__._validate(ms_lh)
        return __class__.firewall_ports[ms_lh]["node-vpn"]

    def mi_vpn_for(ms_lh: str):
        __class__._validate(ms_lh)
        return __class__.firewall_ports[ms_lh]["mi-vpn"]

    def enroll_only_api(ms_lh: str):
        __class__._validate(ms_lh)
        return __class__.firewall_ports[ms_lh]["enroll-only-api"]


def check_prereqs():

    vnet_name = HUB_VNET_NAME
    firewall_name = FIREWALL_NAME
    firewall_public_ipv4_name = FIREWALL_PUBLIC_IP_IPV4_NAME

    # Check VNet doesn't exist
    exists, *_ = az_cli([
        "network", "vnet", "show",
        "--resource-group", RESOURCE_GROUP,
        "--name", vnet_name,
    ])
    if exists:
        raise LookupError(f"VNet '{vnet_name}' already exists in Azure under xm-rg")

    # Check firewall resource itself doesn't exist
    exists, *_ = az_cli([
        "network", "firewall", "show",
        "--resource-group", RESOURCE_GROUP,
        "--name", firewall_name,
    ])
    if exists:
        raise LookupError(f"Firewall '{firewall_name}' already exists in Azure under xm-rg")

    # Check that the public IP address exists
    exists, *_ = az_cli([
        "network", "public-ip", "show",
        "--resource-group", RESOURCE_GROUP,
        "--name", firewall_public_ipv4_name
    ])
    if not exists:
        raise LookupError(f"Public IPv4 address '{firewall_public_ipv4_name}' doesn't exist in Azure under xm-rg")


def create_vnet():
    vnet_name = HUB_VNET_NAME
    vnet_subnet_prefixes = HUB_VNET_ADDR_SPACE
    vnet_hub_subnet_prefixes = HUB_FIREWALL_SUBNET_ADDR_SPACE
    location = LOCATION
    firewall_subnet_name = "AzureFirewallSubnet" # The firewall resource requires a sunet with this exact name to exist
    container_subnet_name = "ContainerSubnet"

    was_cmd_successful, result, _ = az_cli([
        "network", "vnet", "create",
        "--resource-group", RESOURCE_GROUP,
        "--name", vnet_name,
        "--ddos-protection", "false",
        "--address-prefixes", vnet_subnet_prefixes,
        "--location", location,
        "--subnet-name", firewall_subnet_name,
        "--subnet-prefixes", vnet_hub_subnet_prefixes,
        "--tags", "team=xm", f"purpose=msmigrate-hub"
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create VNet '{vnet_name}'. Error: {result}")


def delete_vnet():
    vnet_name = HUB_VNET_NAME
    successful, result, exit_code = az_cli([
        "network", "vnet", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", vnet_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete VNet '{vnet_name}'. Error: {result}")


def create_firewall():

    firewall_name = FIREWALL_NAME
    firewall_ipconfig_name = FIREWALL_IPCONFIG_NAME
    location = LOCATION
    vnet_name = HUB_VNET_NAME
    ipv4_address = FIREWALL_PUBLIC_IP_IPV4_NAME

    was_cmd_successful, result, _ = az_cli([
        "network", "firewall", "create",
        "--resource-group", RESOURCE_GROUP,
        "--location", location,
        "--name", firewall_name,
        # "--firewall-policy", "<name or id>" # I want to manage it with firewall rules not a policy
        "--tags", "team=xm", f"purpose=msmigrate-hub"
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create Firewall '{firewall_name}'. Error: {result}")

    # Add public ipv4 address and it into the HUB vnet
    was_cmd_successful, result, _ = az_cli([
        "network", "firewall", "ip-config", "create",
        "--resource-group", RESOURCE_GROUP,
        "--firewall-name", firewall_name,
        "--name", firewall_ipconfig_name,
        "--public-ip-address", ipv4_address,
        "--vnet-name", vnet_name,
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create Firewall IP configuration '{firewall_ipconfig_name}'. Error: {result}")


def delete_firewall():
    """Delete the firewall resource

    Raises:
        EnvironmentError: [description]
    """
    firewall_name = FIREWALL_NAME
    successful, result, exit_code = az_cli([
        "network", "firewall", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", firewall_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete firewall '{firewall_name}'. Error: {result}")


@dataclass
class DNATRule:
    """These are all for traffic coming from the internet into the firewall
    """
    name: str
    protocol: str
    dst_ports: str
    translated_addr: str
    translated_port: str
    src: str = "*"                          # Any source address
    dst_addr: str = FIREWALL_PUBLIC_IPV4


def add_nat_rules():

    firewall_name = FIREWALL_NAME
    nat_rule_collection_name = FIREWALL_NAT_RULE_COLLECTION_NAME
    nat_rule_collection_priority = 100

    nat_rules = [
        # Quincy
        DNATRule(name="quincy-ssh", protocol="TCP", dst_ports=FirewallPort.ssh_for("quincy"), translated_addr="10.0.1.4", translated_port=22),
        DNATRule(name="quincy-https", protocol="TCP", dst_ports=FirewallPort.https_for("quincy"), translated_addr="10.0.1.4", translated_port=443),
        DNATRule(name="quincy-node-vpn", protocol="UDP", dst_ports=FirewallPort.node_vpn_for("quincy"), translated_addr="10.0.1.4", translated_port=1194),
        DNATRule(name="quincy-mi-vpn", protocol="UDP", dst_ports=FirewallPort.mi_vpn_for("quincy"), translated_addr="10.0.1.4", translated_port=1195),
        DNATRule(name="quincy-enroll-only-api", protocol="TCP", dst_ports=FirewallPort.enroll_only_api("quincy"), translated_addr="10.0.1.4", translated_port=8443),
        # Cheyenne
        DNATRule(name="cheyenne-ssh", protocol="TCP", dst_ports=FirewallPort.ssh_for("cheyenne"), translated_addr="10.0.2.4", translated_port=22),
        DNATRule(name="cheyenne-https", protocol="TCP", dst_ports=FirewallPort.https_for("cheyenne"), translated_addr="10.0.2.4", translated_port=443),
        DNATRule(name="cheyenne-node-vpn", protocol="UDP", dst_ports=FirewallPort.node_vpn_for("cheyenne"), translated_addr="10.0.2.4", translated_port=1194),
        DNATRule(name="cheyenne-mi-vpn", protocol="UDP", dst_ports=FirewallPort.mi_vpn_for("cheyenne"), translated_addr="10.0.2.4", translated_port=1195),
        DNATRule(name="cheyenne-enroll-only-api", protocol="TCP", dst_ports=FirewallPort.enroll_only_api("cheyenne"), translated_addr="10.0.2.4", translated_port=8443),
        # Dublin
        DNATRule(name="dublin-ssh", protocol="TCP", dst_ports=FirewallPort.ssh_for("dublin"), translated_addr="10.0.3.4", translated_port=22),
        DNATRule(name="dublin-https", protocol="TCP", dst_ports=FirewallPort.https_for("dublin"), translated_addr="10.0.3.4", translated_port=443),
        DNATRule(name="dublin-node-vpn", protocol="UDP", dst_ports=FirewallPort.node_vpn_for("dublin"), translated_addr="10.0.3.4", translated_port=1194),
        DNATRule(name="dublin-mi-vpn", protocol="UDP", dst_ports=FirewallPort.mi_vpn_for("dublin"), translated_addr="10.0.3.4", translated_port=1195),
        DNATRule(name="dublin-enroll-only-api", protocol="TCP", dst_ports=FirewallPort.enroll_only_api("dublin"), translated_addr="10.0.3.4", translated_port=8443),
        # Singapore
        DNATRule(name="singapore-ssh", protocol="TCP", dst_ports=FirewallPort.ssh_for("singapore"), translated_addr="10.0.4.4", translated_port=22),
        DNATRule(name="singapore-https", protocol="TCP", dst_ports=FirewallPort.https_for("singapore"), translated_addr="10.0.4.4", translated_port=443),
        DNATRule(name="singapore-node-vpn", protocol="UDP", dst_ports=FirewallPort.node_vpn_for("singapore"), translated_addr="10.0.4.4", translated_port=1194),
        DNATRule(name="singapore-mi-vpn", protocol="UDP", dst_ports=FirewallPort.mi_vpn_for("singapore"), translated_addr="10.0.4.4", translated_port=1195),
        DNATRule(name="singapore-enroll-only-api", protocol="TCP", dst_ports=FirewallPort.enroll_only_api("singapore"), translated_addr="10.0.4.4", translated_port=8443),
    ]

    nat_rules_pb = tqdm(nat_rules)
    for nat_rule in nat_rules_pb:

        nat_rules_pb.set_description(f"Adding '{nat_rule.name}'")

        was_cmd_successful, result, _ = az_cli([
            "network", "firewall", "nat-rule", "create",
            "--resource-group", RESOURCE_GROUP,
            "--firewall-name", firewall_name,
            "--collection-name", nat_rule_collection_name,      # Collection Argument
            "--name", nat_rule.name,
            "--protocols", nat_rule.protocol,
            "--source-addresses", nat_rule.src,
            "--destination-addresses", nat_rule.dst_addr,
            "--destination-ports", nat_rule.dst_ports,
            "--translated-address", nat_rule.translated_addr,
            "--translated-port", nat_rule.translated_port,
        ] + ([
            # These must only be supplied on the first rule insert which creates the collection
            "--action", "Dnat",                                 # Collection Argument
            "--priority", nat_rule_collection_priority,         # Collection Argument
        ] if nat_rule == nat_rules[0] else []))
        if not was_cmd_successful:
            raise EnvironmentError(f"Failed to add DNAT rule '{nat_rule.name}' to collection '{nat_rule_collection_name}' in firewall '{firewall_name}'. Error: {result}")


@dataclass
class NetworkRule:
    """These are for traffic originating at the LH's and routing via the firewalls internal
    private IP.
    """
    name: str
    protocols: str
    src_addr: str
    dst_addr: str
    dst_ports: str


def add_network_rules():
    """These rules allow HTTPS, SSH, MI-VPN and Node-VPN (dunno if this one is really necessary) connectivity
    between the LH's routing via the firewall's private IP.
    """
    firewall_name = FIREWALL_NAME
    network_rule_collection_name = FIREWALL_NETWORK_RULE_COLLECTION_NAME
    network_rule_collection_priority = 100

    network_rules = [
        NetworkRule(name="SSH,HTTPS", protocols="TCP", src_addr="10.0.0.0/16", dst_addr="10.0.0.0/16", dst_ports="*"),
        NetworkRule(name="Node-VPN,MI-VPN", protocols="UDP", src_addr="10.0.0.0/16", dst_addr="10.0.0.0/16", dst_ports="*"),
        NetworkRule(name="Ping", protocols="ICMP", src_addr="10.0.0.0/16", dst_addr="10.0.0.0/16", dst_ports="*"),
        NetworkRule(name="Internet-HTTPS", protocols="TCP", src_addr="10.0.0.0/16", dst_addr="*", dst_ports="443"),
    ]

    network_rules_pb = tqdm(network_rules)
    for network_rule in network_rules_pb:

        network_rules_pb.set_description(f"Adding '{network_rule.name}'")

        args = [
            "network", "firewall", "network-rule", "create",
            "--resource-group", RESOURCE_GROUP,
            "--firewall-name", firewall_name,
            "--name", network_rule.name,
            "--protocols", network_rule.protocols,
            "--source-addresses", network_rule.src_addr,
            "--destination-addresses", network_rule.dst_addr,
            "--destination-ports", network_rule.dst_ports,
            "--collection-name", network_rule_collection_name
        ]

        if network_rule == network_rules[0]:
            # The first rule creates the collection and requires a couple extra args for that
            # Including these args on subsequent rules causes an error
            args += [
                "--action", "Allow",
                "--priority", network_rule_collection_priority,
            ]
        
        was_cmd_successful, result, _ = az_cli(args)
        if not was_cmd_successful:
            raise EnvironmentError(f"Failed to add network rule '{network_rule.name}' to collection '{network_rule_collection_name}' in firewall '{firewall_name}'. Error: {result}")


def peer_vnet(ms_lh: str):
    """Peer the hub vnet to the VNET of the LH
    We need to run the command twice each time with the vnets reversed

    Args:
        ms_lh (str): one of quincy, cheyenne, dublin, singapore

    Raises:
        EnvironmentError: [description]
    """

    hub_vnet_name = HUB_VNET_NAME
    lh_vnet_name = VMResourceNames.get_vnet_name(ms_lh)

    hub_to_lh_peering_name = f"msmigrate-hub-to-lh-{ms_lh}"
    lh_to_hub_peering_name = f"msmigrate-lh-{ms_lh}-to-hub"

    # 1. hub -> lh
    was_cmd_successful, result, _ = az_cli([
        "network", "vnet", "peering", "create",
        "--resource-group", RESOURCE_GROUP,
        "--name", hub_to_lh_peering_name,
        "--vnet-name", hub_vnet_name,
        "--remote-vnet", lh_vnet_name,
        "--allow-forwarded-traffic",
        "--allow-vnet-access",
        # "--allow-gateway-transit",
        
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create VNET peering '{hub_to_lh_peering_name}'. Error: {result}")

    # 2. lh -> hub
    was_cmd_successful, result, _ = az_cli([
        "network", "vnet", "peering", "create",
        "--resource-group", RESOURCE_GROUP,
        "--name", lh_to_hub_peering_name,
        "--vnet-name", lh_vnet_name,
        "--remote-vnet", hub_vnet_name,
        "--allow-forwarded-traffic",
        "--allow-vnet-access",
        # "--allow-gateway-transit", # I dont think i need this
        # "--use-remote-gateways", # I dont think i need this
        
    ])
    if not was_cmd_successful:
        raise EnvironmentError(f"Failed to create VNET peering '{lh_to_hub_peering_name}'. Error: {result}")


def remove_vnet_peering(ms_lh: str):
    """Deletes both side of the peering between LH Vnets

    Args:
        ms_lh (str): [description]
    """
    hub_vnet_name = HUB_VNET_NAME
    lh_vnet_name = VMResourceNames.get_vnet_name(ms_lh)

    hub_to_lh_peering_name = f"msmigrate-hub-to-lh-{ms_lh}"
    lh_to_hub_peering_name = f"msmigrate-lh-{ms_lh}-to-hub"

    # 1. LH -> hub
    successful, result, exit_code = az_cli([
        "network", "vnet", "peering", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", lh_to_hub_peering_name,
        "--vnet-name", lh_vnet_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete VNet peering '{lh_to_hub_peering_name}'. Error: {result}")

    # 2. hub -> LH
    successful, result, exit_code = az_cli([
        "network", "vnet", "peering", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", hub_to_lh_peering_name,
        "--vnet-name", hub_vnet_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete VNet peering '{hub_to_lh_peering_name}'. Error: {result}")


def add_route_table_to_vnet_peer(ms_lh: str):
    """The LH's in these spoke VNets cannot reach each other by default. This
    adds a route to the gateway that resides in the VNet to forward all traffic to the firewalls
    private IP where it then gets forwarded to the LH.

    Args:
        ms_lh (str): [description]
    """

    location = get_azure_region(ms_lh)
    route_table_name = VMResourceNames.get_route_table_name(ms_lh)
    vnet_name = VMResourceNames.get_vnet_name(ms_lh)
    vnet_subnet_name = VMResourceNames.get_subnet_name(ms_lh)
    firewall_private_ip = FIREWALL_PRIVATE_IPV4

    # 1. Create route table
    successful, result, exit_code = az_cli([
        "network", "route-table", "create",
        "--resource-group", RESOURCE_GROUP,
        "--location", location,
        "--name", route_table_name,
        "--disable-bgp-route-propagation", "true",
        "--tags", "team=xm", f"msmigrate-lh-{ms_lh}"
    ])
    if not successful:
        raise EnvironmentError(f"Failed to create route table '{route_table_name}'. Error: {result}")

    # 2. Add default route to firewall
    successful, result, exit_code = az_cli([
        "network", "route-table", "route", "create",
        "--resource-group", RESOURCE_GROUP,
        "--name", "default",
        "--route-table-name", route_table_name,
        "--address-prefix", "0.0.0.0/0", # default route
        "--next-hop-type", "VirtualAppliance",
        "--next-hop-ip-address", firewall_private_ip,
    ])
    if not successful:
        raise EnvironmentError(f"Failed to add rule to route table '{route_table_name}'. Error: {result}")

    # 3. Associate route-table with VNet subnet
    successful, result, exit_code = az_cli([
        "network", "vnet", "subnet", "update",
        "--resource-group", RESOURCE_GROUP,
        "--name", vnet_subnet_name,
        "--vnet-name", vnet_name,
        "--route-table", route_table_name
    ])
    if not successful:
        raise EnvironmentError(f"Failed to associate route table '{route_table_name}' with subnet '{vnet_subnet_name}'. Error: {result}")


def remove_route_table_from_vnet_peer(ms_lh: str):
    """Removes the route table containing routes that forwards via the firewall
    private IP

    Args:
        ms_lh (str): [description]
    """

    route_table_name = VMResourceNames.get_route_table_name(ms_lh)
    vnet_name = VMResourceNames.get_vnet_name(ms_lh)
    vnet_subnet_name = VMResourceNames.get_subnet_name(ms_lh)

    # 1. Disassociate route-table with VNet subnet
    successful, result, exit_code = az_cli([
        "network", "vnet", "subnet", "update",
        "--resource-group", RESOURCE_GROUP,
        "--name", vnet_subnet_name,
        "--vnet-name", vnet_name,
        "--route-table", "" # dissacossiate
    ])
    if not successful:
        raise EnvironmentError(f"Failed to disassociate route table '{route_table_name}' from subnet '{vnet_subnet_name}'. Error: {result}")

    # 2. Delete route table
    successful, result, exit_code = az_cli([
        "network", "route-table", "delete",
        "--resource-group", RESOURCE_GROUP,
        "--name", route_table_name,
    ])
    if not successful:
        raise EnvironmentError(f"Failed to delete route-table '{route_table_name}'. Error: {result}")


def create(ms_lh_vnet_peers: List[str] = []):
    """A good reference for the CLI steps is here: https://docs.microsoft.com/en-us/azure/firewall/deploy-cli#deploy-the-firewall
    This is also another good tutorial: https://docs.microsoft.com/en-us/azure/firewall/tutorial-firewall-dnat
    """

    # 1. Check that firewall and dependent resources are satisfied
    print("Checking firewall pre-requisites...")
    try:
        check_prereqs()
    except LookupError as err:
        sys.exit(f"Firewall prerequisites unsatisfied: {err}")

    print("Pre-requisites satisfied. Creating VNet...")

    # 2. Create VNet
    try:
        create_vnet()
    except EnvironmentError as err:
        sys.exit(f"Failed to create VNet for firewall: {err}")

    print("VNet created. Creating firewall...")

    # 3. Create firewall
    try:
        create_firewall()
    except EnvironmentError as err:
        sys.exit(f"Failed to create firewall resouce: {err}")

    print("Firewall created. Adding network rules...")

    #4. Add Network rules (outbound)
    try:
        add_network_rules()
    except EnvironmentError as err:
        sys.exit(f"Failed to network rules: {err}")

    print("Network rules added. Adding DNAT rules...")

    # 5. Add DNAT rules (inbound)
    try:
        add_nat_rules()
    except EnvironmentError as err:
        sys.exit(f"Failed to add DNAT rules: {err}")

    # 6. Peer VNETs
    if ms_lh_vnet_peers:
        print("Adding VNET peerings...")
        ms_lh_vnet_peers_pb = tqdm(ms_lh_vnet_peers)
        for ms_lh_vnet_peer in ms_lh_vnet_peers_pb:
            ms_lh_vnet_peers_pb.set_description(f"Adding '{ms_lh_vnet_peer}'")
            try:
                peer_vnet(ms_lh_vnet_peer)
            except EnvironmentError as err:
                sys.exit(f"Failed to add VNET peer for '{ms_lh_vnet_peer}': {err}")
    else:
        print("No VNET peering specified")

    # 7. Add route tables to VNets
    if ms_lh_vnet_peers:
        print("Adding route tables to VNets...")
        ms_lh_vnet_peers_pb = tqdm(ms_lh_vnet_peers)
        for ms_lh_vnet_peer in ms_lh_vnet_peers_pb:
            ms_lh_vnet_peers_pb.set_description(f"Adding '{ms_lh_vnet_peer}'")
            try:
                add_route_table_to_vnet_peer(ms_lh_vnet_peer)
            except EnvironmentError as err:
                sys.exit(f"Failed to add route table to '{ms_lh_vnet_peer}': {err}")
    else:
        print("No VNET peers to add route tables to")

    print("Done")


def delete(ms_lh_vnet_peers: List[str] = []):
    """Delete all created resources
    """

    if ms_lh_vnet_peers:
        print("Deleting route tables...")
        ms_lh_vnet_peers_pb = tqdm(ms_lh_vnet_peers)
        for ms_lh_vnet_peer in ms_lh_vnet_peers_pb:
            ms_lh_vnet_peers_pb.set_description(f"Deleting '{ms_lh_vnet_peer}'")
            try:
                remove_route_table_from_vnet_peer(ms_lh_vnet_peer)
            except EnvironmentError as err:
                sys.exit(f"Failed to remove route-table for '{ms_lh_vnet_peer}': {err}")

    if ms_lh_vnet_peers:
        print("Deleting VNET peerings...")
        ms_lh_vnet_peers_pb = tqdm(ms_lh_vnet_peers)
        for ms_lh_vnet_peer in ms_lh_vnet_peers_pb:
            ms_lh_vnet_peers_pb.set_description(f"Deleting '{ms_lh_vnet_peer}'")
            try:
                remove_vnet_peering(ms_lh_vnet_peer)
            except EnvironmentError as err:
                sys.exit(f"Failed to remove VNET peer for '{ms_lh_vnet_peer}': {err}")
    else:
        print("No VNET peering specified for removal")

    print("VNet peerings deleted. Deleting Firewall...")

    try:
        delete_firewall()
    except EnvironmentError as err:
        sys.exit(f"Failed to delete firewall: {err}")

    print("Firewall deleted. Deleting Hub VNet...")

    try:
        delete_vnet()
    except EnvironmentError as err:
        sys.exit(f"Failed to delete firewall resources: {err}")

    print("Done")


def main():
    parser = argparse.ArgumentParser(description="""
Create the firewall in Azure and boot it up.\n"

"""
    )

    # Make --create, --delete flags mutually exclusive
    create_delete_group = parser.add_mutually_exclusive_group(required=True)
    create_delete_group.add_argument("--create", action="store_true")
    create_delete_group.add_argument("--delete", action="store_true")
    parser.add_argument("--vnet_peers", type=str, default=None, help=f"Create/delete these comma seperated list of LH' peer {VALID_MS_LHS}")
    args = parser.parse_args()

    vnet_peers = [x.strip() for x in args.vnet_peers.split(",")] if args.vnet_peers is not None else []
    assert all(ms_lh in VALID_MS_LHS for ms_lh in vnet_peers), f"--vnet_peers contains invalid values {vnet_peers}"

    if args.create:
        create(vnet_peers)
    elif args.delete:
        delete(vnet_peers)
    else:
        # I dunno if we can ever reach this
        raise EnvironmentError("How did this even happen?")



if __name__ == "__main__":
    main()