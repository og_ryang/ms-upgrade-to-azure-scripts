# ms-upgrade-to-azure

## Overview

This repo includes a bunch of scripts i created to help automate a few steps as part of the MS upgrade. I initially looked at using [ansible](https://www.ansible.com/integrations/cloud/microsoft-azure) for automating the deployments of VM's but i don't have access to the Client ID and Client secrets so couldn't take this approach.

Then i looked at using python for VM deployment e.g. https://docs.microsoft.com/en-us/azure/virtual-machines/windows/python but that requires an [active directory service principal](https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal) which I don't have permissions to create.

Instead, i'll use the Azure CLI directly but i'll use python to drive it. I personally really like this method. I can run the commands myself outside of the script to see the outputs and help debug it.

There's also a few diagrams in `diagrams/` that i've attached to JIRA tickets. IDK where else to put them. To edit, use the vscode drawio extension.

## Prerequisites

Make sure you are a part of the `xm-rg` in Azure. All these scripts create resources in there.

Also, ensure you have Azure CLI setup as well. See [confluence](https://opengear.atlassian.net/wiki/spaces/TNGCS/pages/926024021/How+to+signup+get+access+setup+to+Azure+and+Azure+CLI) for details

Create a virtual env and install the pip dependencies like so :
```bash
$ python3.8 -m venv env
$ source env/bin/activate
(env) $ pip install pip -U
(env) $ pip install -r requirements.txt
```

## Scripts

### Uploading LH Images to multiple regions

The `azcopy-lh-img-aus-to-msregions.py` script does this. Provide it the path to the zip file as well as a json file that describes the intermediate and destination storage accounts e.g.

```bash
python scripts/azcopy-lh-img-aus-to-msregions.py -a storage-accounts.json -z /path/to/zip/lighthouse-20.Q3.2-rc1.azure.zip
```

where `storage-accounts.json` is of the format:

```json
{
    "intermediateStorageAccount": {
        "name": "xmstorageacct",
        "writeContainer": "lh-images",
        "sas": "?sv=2019-12-12&ss=bfqt&srt=sco&sp=rwdlacupx&se=2021-02-07T15:19:16Z&st=2021-01-07T07:19:16Z&spr=https&sig=i3jl%2THISISNOTIT%3D"
    },
    "destinationStorageAccounts": [
        {
            "name": "xmstorageaccountdublin",
            "writeContainer": "lh-images",
            "sas": "?sv=2019-12-12&ss=bfqt&srt=sco&sp=rwdlacupx&se=2021-02-11T08:28:08Z&st=2021-01-11T00:28:08Z&spr=https&sig=ORTHIS%3D"
        },
        ...
    ]
}
```

you can use the `sample-storage-accounts.json` as a template for creating this.

The intermediate account is or on-ramp to the azure network. It is best to pick a storage account that provides the fastest upload from the localhost. Once in the intermediate storage account it is copied to all the destination storage accounts. This is pretty quick at this stage as it uses MS's azure backbone network for the transfer. 

The SAS's should be created with all options "allowed services", "allowed resource types" and "allowed permissions" checked.


